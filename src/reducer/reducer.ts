import {
  REQUEST_PRODUCTS,
  SUCCESS_REQUEST_PRODUCTS,
  ADD_TO_CART,
  REMOVE_FROM_CART,
} from "../actions";

const initialState: ProductState = {
  status: "success",
  products: [],
  cart: [],
};

export const reducer = (
  state: ProductState = initialState,
  action: ProductActions
): ProductState => {
  switch (action.type) {
    case REQUEST_PRODUCTS:
      return {
        ...state,
        status: "loading",
      };
    case SUCCESS_REQUEST_PRODUCTS:
      return {
        ...state,
        status: "success",
        products: action.payload as Product[],
      };
    case ADD_TO_CART:
      const product = action.payload as Product;
      const isProductAlreadyInCart = Boolean(
        state.cart.find((prod) => prod.id === product.id)
      );
      if (isProductAlreadyInCart) {
        return {
          ...state,
          cart: state.cart.map((item) =>
            item.id === product.id
              ? { ...item, quantity: item.quantity + 1 }
              : item
          ),
        };
      }
      return {
        ...state,
        cart: [...state.cart, { ...product, quantity: 1 }],
      };
		case REMOVE_FROM_CART:
			const removedProductId = (action.payload as {id: number}).id
			return {
				...state,
				cart: state.cart.filter(item => item.id !== removedProductId)
			}
  }
  return state;
};
