import React from 'react';
import './App.css';
import { MLayout } from './components/Layout/Layout';
import { Products } from './components/Products';
import { Provider } from "react-redux"
import { store } from './store/store';

function App() {
  return (
    <Provider store={store}>
      <MLayout content={<Products/>}/>
    </Provider>
  );
}

export default App;
