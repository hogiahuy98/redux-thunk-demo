type ActionTypes = 'REQUEST_PRODUCTS' | 'SUCCESS_REQUEST_PRODUCTS' | 'ADD_TO_CART' | 'REMOVE_FROM_CART';

type Product = {
    name: string
    price: number
    image: string
    id: number
}

type ProductCartItem = {
    quantity: number
} & Product

type ProductState = {
    status: 'loading' | 'success' | 'error',
    products: Product[],
    cart: ProductCartItem[]
}

type ProductActions = {
    type: ActionTypes,
    payload?: Product[] | Product | {id: number},
}

type DispatchType = (args: ProductActions) => ProductActions;