import { ADD_TO_CART, REQUEST_PRODUCTS, SUCCESS_REQUEST_PRODUCTS } from './actionTypes';
import axios from 'axios';

const client = axios.create({
    baseURL: 'http://localhost:3000'
})

export function getProduct(): any{
    return async (dispatch: any) => {
        dispatch(requestProducts());
        const Products = await client.get<Product[]>('/mock-data/smartphones.json').then(res => res.data);
        dispatch(successRequestProduct(Products));
    }
}

export function requestProducts(): ProductActions {
    return {
        type: REQUEST_PRODUCTS
    }
}

export function successRequestProduct(products: Product[]): ProductActions {
    return {
        type: SUCCESS_REQUEST_PRODUCTS,
        payload: products
    }
}

export function addToCard(product: Product): ProductActions {
    return {
        type: ADD_TO_CART,
        payload: product
    }
}

export function removeFromCard(productId: number): ProductActions {
    return {
        type: 'REMOVE_FROM_CART',
        payload: {
            id: productId
        }
    }
}