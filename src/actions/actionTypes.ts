export const REQUEST_PRODUCTS: ActionTypes = 'REQUEST_PRODUCTS';
export const SUCCESS_REQUEST_PRODUCTS: ActionTypes = 'SUCCESS_REQUEST_PRODUCTS';
export const ADD_TO_CART: ActionTypes = 'ADD_TO_CART'
export const REMOVE_FROM_CART: ActionTypes = 'REMOVE_FROM_CART';