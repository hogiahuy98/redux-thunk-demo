import { Badge, Button, Card, Col, Row } from "antd";
import { useSelector, useDispatch } from "react-redux";
import React from "react";
import { addToCard, getProduct } from "../../actions/actions";
import { ShoppingCartOutlined } from "@ant-design/icons";
import { ProductCard } from "../ProductCard/ProductCard";
import { Cart } from "../Cart/Cart";

export const Products = () => {
  const state: ProductState = useSelector((state: ProductState) => state);

  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(getProduct());
  }, []);

  const cartTotalItem = React.useMemo(
    () => state.cart.reduce<number>((total, item) => total + item.quantity, 0),
    [state.cart]
  );

  const [isOpenCart, setIsOpenCart] = React.useState(false);

  return state.status === "loading" ? (
    <div>Loading</div>
  ) : (
    <>
      <Badge count={cartTotalItem}>
        <Button style={{marginBottom: '32px'}} onClick={() => setIsOpenCart(true)}>
          <ShoppingCartOutlined/>
        </Button>
      </Badge>
      <Row gutter={16}>
        {state.products.map((product) => (
          <Col span={8} key={product.id}>
            <ProductCard product={product} />
          </Col>
        ))}
      </Row>
      <Cart isOpen={isOpenCart} onClose={() => setIsOpenCart(false)} />
    </>
  );
};
