import { DeleteOutlined } from "@ant-design/icons";
import { Button, Col, Descriptions, Modal, Row, Space, Typography } from "antd";
import { useDispatch, useSelector } from "react-redux"
import './Cart.css'
import React from 'react'
import { removeFromCard } from "../../actions/actions";
const { Text } = Typography;

type Props = {
  isOpen: boolean,
  onClose: () => void
}

export const Cart = (props: Props) => {
  const cart = useSelector((state: ProductState) => state.cart);
  const dispatch = useDispatch();

  const totalPrice = React.useMemo(
    () => cart.reduce((totalPr, item) => totalPr + item.price * item.quantity, 0),
  [cart])


  return (
    <Modal open={props.isOpen} onCancel={props.onClose} title="Cart" footer={
      <Text>{`Total: ${totalPrice.toLocaleString()} VND`}</Text>
    }>
      {cart.map((product) => (
        <Row style={{marginBottom: '16px', alignItems: 'center'}}>
          <Col span={4}>
            <img
              src={product.image}
              width={72}
              height={72}
              alt={product.name}
            />
          </Col>
          <Col span={12}>
            <div className="text-center">
              <Text strong>{product.name}</Text>
              <Text>{`${product.price.toLocaleString()} VND`}</Text>
            </div>
          </Col>
          <Col span={4}>
              <Text>{`x ${product.quantity}`}</Text>
          </Col>
          <Col span={4}>
            <Button type="text" danger onClick={() => dispatch(removeFromCard(product.id))}>
              <DeleteOutlined />
            </Button>
          </Col>
        </Row>
      ))}
    </Modal>
  );
}