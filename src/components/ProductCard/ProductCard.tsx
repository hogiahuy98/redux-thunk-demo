import { ShoppingCartOutlined } from "@ant-design/icons";
import { Card } from "antd";
import { useDispatch } from "react-redux";
import { addToCard } from "../../actions/actions";

type Props = {
  product: Product;
};

export const ProductCard = (props: Props) => {
  const { product } = props;

  const dispatch = useDispatch();

  return (
    <Card
      hoverable
      style={{ marginBottom: "16px" }}
      cover={
        <img
          alt="example"
          src={product.image}
          style={{
            height: "300px",
            objectFit: "contain",
          }}
        />
      }
      onClick={() => dispatch(addToCard(product))}
      actions={[<ShoppingCartOutlined />]}
    >
      <Card.Meta
        title={product.name}
        description={`${product.price.toLocaleString()} VND `}
      />
    </Card>
  );
};
